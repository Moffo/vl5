package de.fhbingen.epro.vl5;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.util.Assert;

public class ProjectStartJavaConfig {

	public static void main(String[] args) {
		ApplicationContext ctx = 
			      new AnnotationConfigApplicationContext(BaseConfiguration.class);
		
		Assert.notNull(ctx);
	}
}
